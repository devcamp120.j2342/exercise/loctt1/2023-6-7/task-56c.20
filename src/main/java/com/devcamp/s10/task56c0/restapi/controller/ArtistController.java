package com.devcamp.s10.task56c0.restapi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s10.task56c0.restapi.models.Artist;
import com.devcamp.s10.task56c0.restapi.services.ArtistService;

@RestController
@CrossOrigin

public class ArtistController {
     @Autowired
     private ArtistService artistService;

     @GetMapping("/artists")
     public ArrayList<Artist> getAllArtist(){
          return artistService.getAllArtist();
     }

     @GetMapping("/artist-info/{artistId}")
     public Artist getArtistInfo(@PathVariable int artistId){
          return artistService.getArtistById(artistId);
     }

}
