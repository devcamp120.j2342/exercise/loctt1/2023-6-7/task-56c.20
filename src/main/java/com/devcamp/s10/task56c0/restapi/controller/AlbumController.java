package com.devcamp.s10.task56c0.restapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s10.task56c0.restapi.models.Album;
import com.devcamp.s10.task56c0.restapi.services.AlbumService;

@RestController
@CrossOrigin

public class AlbumController {
     @Autowired
     private AlbumService albumService;
     
     @GetMapping("/album-info/{albumId}")
     public Album getAlbumByInfo(@PathVariable int albumId){
          return albumService.getAlbumById(albumId);
     }
}
