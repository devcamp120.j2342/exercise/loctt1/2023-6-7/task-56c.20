package com.devcamp.s10.task56c0.restapi.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.s10.task56c0.restapi.models.Album;
import com.devcamp.s10.task56c0.restapi.models.Artist;

@Service
public class ArtistService {
     private ArrayList<Artist> artists;

     public ArtistService(){
          //khởi tạo bộ dữ liệu 
          artists = new ArrayList<>();

          //Tạo danh sách album 
          ArrayList<String> artistSong = new ArrayList<>();
          artistSong.add("Bai hat 1");
          artistSong.add("Bai hat 2");

          ArrayList<Album> artistAlbum = new ArrayList<>();
          artistAlbum.add(new Album(1, "Album 1", artistSong));
          artistAlbum.add(new Album(2, "Album 2", artistSong));
          //Tạo danh sách artist
          Artist artist1 = new Artist(1, "Tan Loc", artistAlbum);     
          artists.add(artist1);

          ArrayList<String> artist2Songs = new ArrayList<>();
          artist2Songs.add("Bai hat 3");
          ArrayList<Album> artist2Album = new ArrayList<>();
          artist2Album.add(new Album(3, "Album 3", artist2Songs));
          Artist artist2 = new Artist(2, "My Cam", artist2Album);
          artists.add(artist2);
          

     }

     public ArrayList<Artist> getAllArtist(){
          return artists;
     }

     public Artist getArtistById(int artistId){
          for(Artist artist : artists){
               if(artist.getId() == artistId){
                    return artist;
               }
          }
          return null;
     }
}
