package com.devcamp.s10.task56c0.restapi.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.s10.task56c0.restapi.models.Album;
@Service
public class AlbumService {
     private ArrayList<Album> albums;

     public AlbumService(){
          //khởi tạo dữ liệu 
          albums = new ArrayList<>();
          
          albums.add(new Album(1, "Album 1"));
          albums.add(new Album(2, "Album 2"));
          albums.add(new Album(3, "Album 3"));

     }

     public Album getAlbumById(int albumId){
          for(Album album : albums){
               if(album.getId() == albumId){
                    return album;
               }
          }
          return null;
     }
}
